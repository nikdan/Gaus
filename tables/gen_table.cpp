#include <string>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <ctime>

using std::vector;

const size_t max_weight = 20000;
size_t dim = 5;
size_t amount = 5;

vector< vector<double> > fill ()
{
	srand (time(NULL));
	vector< vector<double> > arr;
	arr.resize (amount);
	for (size_t i = 0; i < amount; ++i)
	{
		arr[i].resize (dim+1);
		for (size_t j = 0; j < dim+1; ++j) {
			arr[i][j] = (double)(rand() % max_weight) / 2;
		}
	}
	return arr;
}



void print_table (std::ofstream& out, vector< vector<double> >& arr)
{
	for (size_t i = 0; i < arr.size(); ++i)
	{
		for (size_t j = 0; j < arr[i].size(); ++j)
		{
			out << std::setw(10) << arr[i][j];
		}
		out << '\n';
	}
}


int main (int argc, char *argv[])
{
	std::string filename ("in_5.dat");
	if (argc > 2)
	{
		filename = argv[1];
		amount = atoi (argv[2]);
		dim = atoi (argv[3]);
	}
	std::ofstream out ( filename.c_str() );
	vector< vector<double> > arr = fill();

	out << std::setw(4) << amount << std::setw(4) << dim << '\n';
	print_table (out, arr);
}
